import {
  UserInformationParam,
  UserRepositoryInterface,
} from './user-repository-interface';
import { UserClass } from '../class/User';
import { strRandom } from '../service/common';

export const USER_REPOSITORY: UserRepositoryInterface = () => {
  return {
    getInstance: () => {
      return {
        getList: () => USER_REPOSITORY_INIT,
        findUser: (idParam: string) =>
          findUserWithInstance(idParam, USER_REPOSITORY_INIT),
        createUser: (userInformationParam: UserInformationParam) =>
          CreateUserWithInstance(userInformationParam, USER_REPOSITORY_INIT),
        editUser: (userParam: UserClass) =>
          UpdateUserWithInstance(userParam, USER_REPOSITORY_INIT),
        deleteUser: (idParam: string) =>
          deleteUserWithInstance(idParam, USER_REPOSITORY_INIT),
      };
    },
  };
};

export const findUserWithInstance = (
  idParam: string,
  instance: Array<UserClass>
) => {
  return instance.find((user) => {
    return idParam === user.id;
  });
};

export const CreateUserWithInstance = (
  userInformationParam: UserInformationParam,
  instance: Array<UserClass>
) => {
  try {
    let newUser: UserClass = new UserClass(
      strRandom(),
      userInformationParam.name,
      userInformationParam.surname,
      userInformationParam.cell,
      userInformationParam.phone
    );
    instance.push(newUser);
    return newUser;
  } catch (e) {
    console.error("Erreur lors de la création de l'utilisateur");
    return undefined;
  }
};

export const UpdateUserWithInstance = (
  userParam: UserClass,
  instance: Array<UserClass>
) => {
  try {
    let userToModifyIndex = instance.findIndex((user) => {
      return userParam.id === user.id;
    });
    if (userToModifyIndex === -1) {
      throw new Error("l'utilisateur est introuvable");
    }
    instance[userToModifyIndex] = userParam;
    return true;
  } catch (e) {
    console.error("Erreur lors de la modification de l'utilisateur");
    return false;
  }
};

export const deleteUserWithInstance = (
  idParam: string,
  instance: Array<UserClass>
) => {
  try {
    let userToDeleteIndex = instance.findIndex((user) => {
      return idParam === user.id;
    });
    if (userToDeleteIndex === -1) {
      throw new Error("l'utilisateur est introuvable");
    }
    instance = instance.splice(userToDeleteIndex, 1);
    return true;
  } catch (e) {
    console.error("Erreur lors de la supréssion de l'utilisateur");
    return false;
  }
};

export const USER_REPOSITORY_INIT: Array<UserClass> = [
  {
    id: 'tintin1',
    name: 'tintin',
    surname: 'HERGER',
    cell: '01 02 03 04 05',
    phone: '01 02 03 04 05',
  },
  {
    id: 'tintin2',
    name: 'milou',
    surname: 'HERGER',
    cell: '01 02 03 04 05',
    phone: '01 02 03 04 05',
  },
  {
    id: 'tintin3',
    name: 'hadock',
    surname: 'HERGER',
    cell: '01 02 03 04 05',
    phone: '01 02 03 04 05',
  },
];
