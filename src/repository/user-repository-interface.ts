import { UserClass } from '../class/User';

export interface UserInformationParam {
  name: string;
  surname: string;
  cell: string;
  phone: string;
}

export type UserRepositoryInterface = () => {
  getInstance: () => {
    getList: () => Array<UserClass>;
    findUser: (idParam: string) => UserClass | undefined;
    createUser: (
      userInformationParam: UserInformationParam
    ) => UserClass | undefined;
    editUser: (userParam: UserClass) => boolean;
    deleteUser: (idParam: string) => boolean;
  };
};
