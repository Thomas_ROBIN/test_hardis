import axios from 'axios';
import { UserInformationParam } from '../repository/user-repository-interface';
const API_RAMDOM_USER =
  'https://randomuser.me/api/?results=1&nat=fr&inc=name,cell,phone';

export const getRandomUserFromAPI: () => Promise<UserInformationParam | null> = () => {

  return axios.get(API_RAMDOM_USER)
  .then((response) => {
    return {
      name: response.data.results[0].name.first,
      surname: response.data.results[0].name.last,
      cell: response.data.results[0].cell,
      phone: response.data.results[0].phone
    }
  }).catch(e => {
    console.error('une erreur est survenue lors de la requete : ' + e);
    return null;
  })
};
