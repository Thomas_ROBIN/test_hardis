// Generate ramdom string
export function strRandom() {
  var c = '',
    d = 0,
    e = 'abcdefghijklmnopqrstuvwxyz';
  for (; d < 10; d++) {
    c += e[Math.floor(Math.random() * e.length)];
  }
  return c;
}
