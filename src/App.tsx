import React from "react";
import { CreateUpdateUserComponent } from "./component/user/createAndUpdate/CreateUpdateUserComponent";
import { USER_REPOSITORY } from "./repository/user-repository";
import { UserListComponent } from "./component/user/list/userList";
import "react-sliding-pane/dist/react-sliding-pane.css";
import "bootstrap/dist/css/bootstrap.min.css";

function App() {
  const [userList, setUserList] = React.useState(
    USER_REPOSITORY().getInstance().getList()
  );
  return (
    <div className="App">
      <header className="App-header">
        <h1>Test Hardis</h1>
      </header>
      <br />
      <div>
        <CreateUpdateUserComponent
          {...{
            updateList: () => {
              setUserList([...USER_REPOSITORY().getInstance().getList()]);
            },
          }}
        />
        <br />
        <UserListComponent
          {...{
            updateList: () => {
              setUserList([...USER_REPOSITORY().getInstance().getList()]);
            },
            userList,
          }}
        />
      </div>
    </div>
  );
}

export default App;
