import React from "react";
import SlidingPane from "react-sliding-pane";
import { UserClass } from "../../../class/User";
import { CreateUpdateUserComponent } from "./CreateUpdateUserComponent";

export const PaneUpdateUserComponent = (props: {
  updateList: () => void;
  isOpenSlide: boolean;
  setIsOpenSlide: (value: boolean) => void;
  userProps: UserClass;
}) => {
  return (
    <div>
      <SlidingPane
        className="some-custom-class"
        overlayClassName="some-custom-overlay-class"
        isOpen={props.isOpenSlide}
        onRequestClose={() => props.setIsOpenSlide(false)}
        title="Modification d'un utilisateur"
      >
        <CreateUpdateUserComponent
          {...{
            updateList: () => {
              props.setIsOpenSlide(false);
              props.updateList();
            },
            userProps: props.userProps,
          }}
        />
        <br />
      </SlidingPane>
    </div>
  );
};
