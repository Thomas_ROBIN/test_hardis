import React from "react";
import { USER_REPOSITORY } from "../../../repository/user-repository";
import { getRandomUserFromAPI } from "../../../service/apiRandomUser";
import { UserClass } from "../../../class/User";
import { Button } from "react-bootstrap";

export const CreateUpdateUserComponent = (props: {
  updateList: () => void;
  userProps?: UserClass;
}) => {
  const [name, setName] = React.useState(
    props.userProps ? props.userProps.name : ""
  );
  const [surname, setSurname] = React.useState(
    props.userProps ? props.userProps.surname : ""
  );
  const [cell, setCell] = React.useState(
    props.userProps ? props.userProps.cell : ""
  );
  const [phone, setPhone] = React.useState(
    props.userProps ? props.userProps.phone : ""
  );

  const resetForm = () => {
    if (props.userProps === undefined) {
      setName("");
      setSurname("");
      setCell("");
      setPhone("");
    } else {
      setName(props.userProps.name);
      setSurname(props.userProps.surname);
      setCell(props.userProps.cell);
      setPhone(props.userProps.phone);
    }
  };

  const validateForm = () => {
    if (props.userProps === undefined) {
      USER_REPOSITORY().getInstance().createUser({
        name,
        surname,
        cell,
        phone,
      });
    } else {
      USER_REPOSITORY().getInstance().editUser({
        id: props.userProps.id,
        name,
        surname,
        cell,
        phone,
      });
    }
    props.updateList();
  };

  return (
    <div className="CreateUpdateUserComponent">
      {props.userProps === undefined && <div>Création d'un utilisateur</div>}

      {props.userProps === undefined && (
        <Button
          variant="primary"
          onClick={() => {
            getRandomUserFromAPI().then((result) => {
              if (result !== null) {
                USER_REPOSITORY().getInstance().createUser(result);
                props.updateList();
              }
            });
          }}
        >
          Ajouter un utilisateur aléatoire
        </Button>
      )}
      <div className="input-name">
        <span>Prénom:</span>
        <input
          type="text"
          name="name"
          id="name"
          title="name"
          placeholder="Dupont"
          value={name}
          onChange={(e) => {
            setName(e.target.value);
          }}
        />
      </div>

      <div className="input-surname">
        <span>Nom de famille:</span>
        <input
          type="text"
          name="name"
          id="surname"
          title="surname"
          placeholder="Jean"
          value={surname}
          onChange={(e) => {
            setSurname(e.target.value);
          }}
        />
      </div>

      <div className="input-cell">
        <span>Telephone fixe:</span>
        <input
          type="text"
          name="cell"
          id="cell"
          title="cell"
          placeholder="0102030405"
          value={cell}
          onChange={(e) => {
            setCell(e.target.value);
          }}
        />
      </div>

      <div className="input-phone">
        <span>Téléphone portable:</span>
        <input
          type="text"
          name="phone"
          id="phone"
          title="phone"
          placeholder="0102030405"
          value={phone}
          onChange={(e) => {
            setPhone(e.target.value);
          }}
        />
      </div>

      <Button variant="danger" onClick={() => resetForm()}>
        Reset
      </Button>

      <Button
        variant="success"
        onClick={(e) => {
          validateForm();
        }}
      >
        Valider
      </Button>
    </div>
  );
};
