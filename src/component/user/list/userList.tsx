import React from "react";
import { UserClass } from "../../../class/User";
import { USER_REPOSITORY } from "../../../repository/user-repository";
import { PaneUpdateUserComponent } from "../createAndUpdate/paneUpdateUser";
import { Button, Table } from "react-bootstrap";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faTrash, faPen } from "@fortawesome/free-solid-svg-icons";

export const UserListComponent = (props: {
  updateList: () => void;
  userList: Array<UserClass>;
}) => {
  return (
    <div>
      <div> Liste des utilisateurs </div>
      <Table>
        <thead>
          <tr>
            <th>Prénom</th>
            <th>Nom</th>
            <th>Téléphone fixe</th>
            <th>Téléphone portable</th>
            <th>actions</th>
          </tr>
        </thead>
        <tbody>
          {props.userList.map((userItem, index) => {
            return (
              <UserLineListComponent
                key={index}
                {...{ updateList: props.updateList, userItem }}
              />
            );
          })}
        </tbody>
      </Table>
    </div>
  );
};

export const UserLineListComponent = (props: {
  updateList: () => void;
  userItem: UserClass;
}) => {
  return (
    <tr>
      <td>
        {props.userItem.name.slice(0, 1).toUpperCase() +
          props.userItem.name.slice(1).toLocaleLowerCase()}
      </td>
      <td>{props.userItem.surname.toUpperCase()}</td>
      <td>{props.userItem.cell}</td>
      <td>{props.userItem.phone}</td>
      <td>
        <UserLineActionComponent
          {...{ updateList: props.updateList, userItem: props.userItem }}
        />
      </td>
    </tr>
  );
};

export const UserLineActionComponent = (props: {
  updateList: () => void;
  userItem: UserClass;
}) => {
  const [isOpenPane, setIsOpenPane] = React.useState(false);
  return (
    <div>
      <Button
        variant="success"
        onClick={() => {
          setIsOpenPane(true);
        }}
      >
        <FontAwesomeIcon icon={faPen} />
      </Button>
      <Button
        variant="danger"
        onClick={() => {
          USER_REPOSITORY().getInstance().deleteUser(props.userItem.id);
          props.updateList();
        }}
      >
        <FontAwesomeIcon icon={faTrash} />
      </Button>
      <PaneUpdateUserComponent
        {...{
          updateList: props.updateList,
          isOpenSlide: isOpenPane,
          setIsOpenSlide: setIsOpenPane,
          userProps: props.userItem,
        }}
      />
    </div>
  );
};
