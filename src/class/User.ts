export class UserClass {
    id: string;
    name: string;
    surname: string;
    cell: string;
    phone: string;

    constructor(idParam: string, nameParam: string, surnameParam: string, cellParam: string, phoneParam: string, ) {
        this.id = idParam;
        this.name = nameParam;
        this.surname = surnameParam;
        this.cell = cellParam;
        this.phone = phoneParam;
      }
}